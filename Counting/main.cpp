// main.cpp

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

#include<iostream>
#include<conio.h>           // it may be necessary to change or remove this line if not using Windows

#include "Blob.h"

#define SHOW_STEPS            // un-comment or comment this line to show steps or not

// Kolory /////////////////////////////////////////////////////////////////////////
const cv::Scalar SCALAR_BLACK = cv::Scalar(0.0, 0.0, 0.0);
const cv::Scalar SCALAR_WHITE = cv::Scalar(255.0, 255.0, 255.0);
const cv::Scalar SCALAR_YELLOW = cv::Scalar(0.0, 255.0, 255.0);
const cv::Scalar SCALAR_GREEN = cv::Scalar(0.0, 200.0, 0.0);
const cv::Scalar SCALAR_RED = cv::Scalar(0.0, 0.0, 255.0);

// Prototypy funkcji //////////////////////////////////////////////////////////////
void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &existingBlobs, std::vector<Blob> &currentFrameBlobs);
void addBlobToExistingBlobs(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs, int &intIndex);
void addNewBlob(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs);
double distanceBetweenPoints(cv::Point point1, cv::Point point2);
void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName);
void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName);
bool checkIfBlobsCrossedTheLine(std::vector<Blob> &blobs, int &intHorizontalLinePosition,int &intBorderLinePosition, int carCount[], std::string &fileName);
void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy);
void drawCarCountOnImage(int carCount[], cv::Mat &imgFrame2Copy);

///////////////////////////////////////////////////////////////////////////////////
int main(void) {

	cv::VideoCapture capVideo;

	cv::Mat imgFrame1;
	cv::Mat imgFrame2;

	std::vector<Blob> blobs;

	cv::Point crossingLine[2];
	cv::Point borderLine[2];

	int carCount[2] = {0, 0};


	std::string fileName;
	//fileName = "frog.mp4";
	//fileName = "auta.mp4";
	std::cout << "Podaj nazwe pliku \n";
	std::cin >> fileName;
	capVideo.open(fileName);

	if (!capVideo.isOpened()) {                                                 // if unable to open video file
		std::cout << "Nie mo�na wczyta� pliku." << std::endl << std::endl;      // show error message
		_getch();                   // it may be necessary to change or remove this line if not using Windows
		return(0);                                                              // and exit program
	}

	if (capVideo.get(CV_CAP_PROP_FRAME_COUNT) < 2) {
		std::cout << "B��d! Plik video musi mie� co najmniej 2 klatki.";
		_getch();                   // it may be necessary to change or remove this line if not using Windows
		return(0);
	}

	// Dwie klatki do porownywania ////////////////////////////////////////////////
	capVideo.read(imgFrame1);
	capVideo.read(imgFrame2);



	// Tworzenie linii "zliczajacej" //////////////////////////////////////////////
	int intHorizontalLinePosition=0;
	int intBorderLinePosition = 0;
	if (fileName == "auta.mp4") {
		intHorizontalLinePosition = (int)std::round((double)imgFrame1.rows * 0.35);

		crossingLine[0].x = 0;
		crossingLine[0].y = intHorizontalLinePosition;

		crossingLine[1].x = imgFrame1.cols - 1;
		crossingLine[1].y = intHorizontalLinePosition;
	}
	else {
		intHorizontalLinePosition = (int)std::round((double)imgFrame1.cols * 0.5);

		crossingLine[0].x = intHorizontalLinePosition;
		crossingLine[0].y = 0;

		crossingLine[1].x = intHorizontalLinePosition;
		crossingLine[1].y = imgFrame1.rows - 1;
		
		intBorderLinePosition = imgFrame1.rows * 0.4;
		borderLine[0].x = 0;
		borderLine[0].y = intBorderLinePosition;

		borderLine[1].x = imgFrame1.cols - 1;
		borderLine[1].y = intBorderLinePosition;
	}

	char chCheckForEscKey = 0;

	bool blnFirstFrame = true;

	int frameCount = 2;

	// Petla while ////////////////////////////////////////////////////////////////
	while (capVideo.isOpened() && chCheckForEscKey != 27) {

		// Deklaracja zmiennej zawierajacej Bloby /////////////////////////////////
		std::vector<Blob> currentFrameBlobs;

		// Kopia klatek ///////////////////////////////////////////////////////////
		cv::Mat imgFrame1Copy = imgFrame1.clone();
		cv::Mat imgFrame2Copy = imgFrame2.clone();

		cv::Mat imgDifference;
		cv::Mat imgThresh;

		// Konwersja do skali szarosci ////////////////////////////////////////////
		cv::cvtColor(imgFrame1Copy, imgFrame1Copy, CV_BGR2GRAY);
		cv::cvtColor(imgFrame2Copy, imgFrame2Copy, CV_BGR2GRAY);

		// Wygladzanie ////////////////////////////////////////////////////////////
		cv::GaussianBlur(imgFrame1Copy, imgFrame1Copy, cv::Size(5, 5), 0);
		cv::GaussianBlur(imgFrame2Copy, imgFrame2Copy, cv::Size(5, 5), 0);

		// Obliczanie roznicy i progowanie ////////////////////////////////////////
		cv::absdiff(imgFrame1Copy, imgFrame2Copy, imgDifference);
		cv::imshow("imgDiff", imgDifference);
		cv::threshold(imgDifference, imgThresh, 30, 255.0, CV_THRESH_BINARY);

		// Wyswietlenie ///////////////////////////////////////////////////////////
		cv::imshow("imgThresh", imgThresh);

		cv::Mat structuringElement3x3 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
		cv::Mat structuringElement5x5 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
		cv::Mat structuringElement7x7 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));
		cv::Mat structuringElement13x13 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(13, 13));
		cv::Mat structuringElement15x15 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));

		// Uwydatnianie ruchomych elementow poprzez dylatacje i erozje ////////////
		if (fileName == "auta.mp4") {
			for (unsigned int i = 0; i < 2; i++) {
				cv::dilate(imgThresh, imgThresh, structuringElement5x5);
				cv::dilate(imgThresh, imgThresh, structuringElement5x5);
				cv::erode(imgThresh, imgThresh, structuringElement5x5);
			}
		}
		else {
			for (unsigned int i = 0; i < 2; i++) {
				cv::dilate(imgThresh, imgThresh, structuringElement13x13);
				cv::dilate(imgThresh, imgThresh, structuringElement13x13);
				cv::erode(imgThresh, imgThresh, structuringElement13x13);
			}
		}

		cv::imshow("imgDilated", imgThresh);

		// Szukanie konturow //////////////////////////////////////////////////////
		cv::Mat imgThreshCopy = imgThresh.clone();

		std::vector<std::vector<cv::Point> > contours;

		cv::findContours(imgThreshCopy, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

		drawAndShowContours(imgThresh.size(), contours, "imgContours");

		// Uwypuklanie konturow ///////////////////////////////////////////////////
		std::vector<std::vector<cv::Point> > convexHulls(contours.size());

		for (unsigned int i = 0; i < contours.size(); i++) {
			cv::convexHull(contours[i], convexHulls[i]);
		}
		//sprawdzam czy komituje
		drawAndShowContours(imgThresh.size(), convexHulls, "imgConvexHulls");

		// Szukanie Bloba, czyli auta /////////////////////////////////////////////
		if (fileName == "auta.mp4") {
			for (auto &convexHull : convexHulls) {
				Blob possibleBlob(convexHull);

				if (possibleBlob.currentBoundingRect.area() > 400 &&	//Pole prostokata
					possibleBlob.dblCurrentAspectRatio > 0.2 &&			//Szerokosc/wysokosc
					possibleBlob.dblCurrentAspectRatio < 4.0 &&
					possibleBlob.currentBoundingRect.width > 30 &&		//Szerokosc prostokata
					possibleBlob.currentBoundingRect.height > 30 &&		//Wysokosc prostokata
					possibleBlob.dblCurrentDiagonalSize > 60.0 &&		//Przekatna
					(cv::contourArea(possibleBlob.currentContour) / (double)possibleBlob.currentBoundingRect.area()) > 0.50) {
					currentFrameBlobs.push_back(possibleBlob);
				}
			}
		}
		else {
			for (auto &convexHull : convexHulls) {
				Blob possibleBlob(convexHull);

				if (possibleBlob.currentBoundingRect.area() > 30000 && 
					possibleBlob.dblCurrentAspectRatio > 1.0 &&
					possibleBlob.dblCurrentAspectRatio < 2.8 && //Bylo 4.0
					possibleBlob.currentBoundingRect.width > 210 &&
					possibleBlob.currentBoundingRect.height > 130 &&
					possibleBlob.dblCurrentDiagonalSize > 250.0 &&
					(cv::contourArea(possibleBlob.currentContour) / (double)possibleBlob.currentBoundingRect.area()) > 0.50) {
					currentFrameBlobs.push_back(possibleBlob);
				}
			}
		}

		drawAndShowContours(imgThresh.size(), currentFrameBlobs, "imgCurrentFrameBlobs");

		// Porownywanie nowych Blobow z istniejacymi, aby usunac nieruchome obiekty /
		if (blnFirstFrame == true) {
			for (auto &currentFrameBlob : currentFrameBlobs) {
				blobs.push_back(currentFrameBlob);
			}
		}
		else {
			matchCurrentFrameBlobsToExistingBlobs(blobs, currentFrameBlobs);
		}

		drawAndShowContours(imgThresh.size(), blobs, "imgBlobs");

		// Rysowanie prostokata wokol Bloba/////////////////////////////////////
		imgFrame2Copy = imgFrame2.clone();          

		drawBlobInfoOnImage(blobs, imgFrame2Copy);

		// Sprawdzanie czy przekroczono linie //////////////////////////////////
		bool blnAtLeastOneBlobCrossedTheLine = checkIfBlobsCrossedTheLine(blobs, intHorizontalLinePosition, intBorderLinePosition, carCount, fileName);
		
		//Dodawanie linii do wyswietlenia
		if (blnAtLeastOneBlobCrossedTheLine == true) {
			cv::line(imgFrame2Copy, crossingLine[0], crossingLine[1], SCALAR_GREEN, 2);
		}
		else {
			cv::line(imgFrame2Copy, crossingLine[0], crossingLine[1], SCALAR_RED, 2);
		}
		//Dodanie linii horyzontalnej do froga, by uniknac glupiego liczenia
		if (fileName == "frog.mp4") {
			//cv::line(imgFrame2Copy, borderLine[0], borderLine[1], SCALAR_RED, 2);
		}
		drawCarCountOnImage(carCount, imgFrame2Copy);

		cv::imshow("imgFrame2Copy", imgFrame2Copy);

		//cv::waitKey(0);                 // uncomment this line to go frame by frame for debugging

		// now we prepare for the next iteration

		currentFrameBlobs.clear();

		imgFrame1 = imgFrame2.clone();           // move frame 1 up to where frame 2 is

		if ((capVideo.get(CV_CAP_PROP_POS_FRAMES) + 1) < capVideo.get(CV_CAP_PROP_FRAME_COUNT)) {
			capVideo.read(imgFrame2);
		}
		else {
			std::cout << "end of video\n";
			break;
		}

		blnFirstFrame = false;
		frameCount++;
		chCheckForEscKey = cv::waitKey(1);
	}

	if (chCheckForEscKey != 27) {               // if the user did not press esc (i.e. we reached the end of the video)
		cv::waitKey(0);                         // hold the windows open to allow the "end of video" message to show
	}
	// note that if the user did press esc, we don't need to hold the windows open, we can simply let the program end which will close the windows

	return(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &existingBlobs, std::vector<Blob> &currentFrameBlobs) {

	// Ustawianie wartosci istniejacych Blobow przed porownywaniem /////////////
	for (auto &existingBlob : existingBlobs) {

		existingBlob.blnCurrentMatchFoundOrNewBlob = false;

		existingBlob.predictNextPosition();
	}

	// Sprawdzanie czy Blob jest nowy, czy nie /////////////////////////////////
	for (auto &currentFrameBlob : currentFrameBlobs) {

		int intIndexOfLeastDistance = 0;
		double dblLeastDistance = 100000.0;

		for (unsigned int i = 0; i < existingBlobs.size(); i++) {

			if (existingBlobs[i].blnStillBeingTracked == true) {

				double dblDistance = distanceBetweenPoints(currentFrameBlob.centerPositions.back(), existingBlobs[i].predictedNextPosition);

				if (dblDistance < dblLeastDistance) {
					dblLeastDistance = dblDistance;
					intIndexOfLeastDistance = i;
				}
			}
		}
		// Tworzy nowego Bloba lub aktualizuje istniejace ///////////////////////
		if (dblLeastDistance < currentFrameBlob.dblCurrentDiagonalSize * 0.5) {
			addBlobToExistingBlobs(currentFrameBlob, existingBlobs, intIndexOfLeastDistance);
		}
		else {
			addNewBlob(currentFrameBlob, existingBlobs);
		}

	}

	// Usuwanie Blobow, ktore uciekly poza ekran (nie zostaly zaktualizowane)////
	for (auto &existingBlob : existingBlobs) {

		if (existingBlob.blnCurrentMatchFoundOrNewBlob == false) {
			existingBlob.intNumOfConsecutiveFramesWithoutAMatch++;
		}

		if (existingBlob.intNumOfConsecutiveFramesWithoutAMatch >= 5) {
			existingBlob.blnStillBeingTracked = false;
		}

	}

}

///////////////////////////////////////////////////////////////////////////////////////////////////
void addBlobToExistingBlobs(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs, int &intIndex) {

	existingBlobs[intIndex].currentContour = currentFrameBlob.currentContour;
	existingBlobs[intIndex].currentBoundingRect = currentFrameBlob.currentBoundingRect;

	existingBlobs[intIndex].centerPositions.push_back(currentFrameBlob.centerPositions.back());

	existingBlobs[intIndex].dblCurrentDiagonalSize = currentFrameBlob.dblCurrentDiagonalSize;
	existingBlobs[intIndex].dblCurrentAspectRatio = currentFrameBlob.dblCurrentAspectRatio;

	existingBlobs[intIndex].blnStillBeingTracked = true;
	existingBlobs[intIndex].blnCurrentMatchFoundOrNewBlob = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void addNewBlob(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs) {

	currentFrameBlob.blnCurrentMatchFoundOrNewBlob = true;

	existingBlobs.push_back(currentFrameBlob);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
double distanceBetweenPoints(cv::Point point1, cv::Point point2) {

	int intX = abs(point1.x - point2.x);
	int intY = abs(point1.y - point2.y);

	return(sqrt(pow(intX, 2) + pow(intY, 2)));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName) {
	cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);

	cv::drawContours(image, contours, -1, SCALAR_WHITE, -1); //-1 oznaczaja, ze rysujemy wszystkie punkty ze zmiennej contours wraz z wypelnieniem

	cv::imshow(strImageName, image);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName) {

	cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);

	std::vector<std::vector<cv::Point> > contours;

	for (auto &blob : blobs) {
		if (blob.blnStillBeingTracked == true) {
			contours.push_back(blob.currentContour);
		}
	}

	cv::drawContours(image, contours, -1, SCALAR_WHITE, -1);

	cv::imshow(strImageName, image);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
bool checkIfBlobsCrossedTheLine(std::vector<Blob> &blobs, int &intHorizontalLinePosition, int &intBorderLinePosition, int carCount[], std::string &fileName) {
	bool blnAtLeastOneBlobCrossedTheLine = false;

	if (fileName == "auta.mp4") {
		for (auto blob : blobs) {

			if (blob.blnStillBeingTracked == true && blob.centerPositions.size() >= 2) {
				int prevFrameIndex = (int)blob.centerPositions.size() - 2;
				int currFrameIndex = (int)blob.centerPositions.size() - 1;

				if (blob.centerPositions[prevFrameIndex].y > intHorizontalLinePosition && blob.centerPositions[currFrameIndex].y <= intHorizontalLinePosition) {
					carCount[0]++;
					blnAtLeastOneBlobCrossedTheLine = true;
				}
			}

		}
	}
	//Sprawdzamy froga
	else {
		for (auto blob : blobs) {
			if (blob.blnStillBeingTracked == true && blob.centerPositions.size() >= 2) {
				int prevFrameIndex = (int)blob.centerPositions.size() - 2;
				int currFrameIndex = (int)blob.centerPositions.size() - 1;

				//Jesli najpierw bylo po lewej, pozniej po prawej
				if (blob.centerPositions[prevFrameIndex].x < intHorizontalLinePosition && blob.centerPositions[currFrameIndex].x >= intHorizontalLinePosition) {
					carCount[0]++;
					blnAtLeastOneBlobCrossedTheLine = true;
					std::cout <<carCount[0]<<"\n"<< "AspectRatio: " << blob.dblCurrentAspectRatio << "\n" << "BoundingRect Area: " << blob.currentBoundingRect.area() << "\n" 
						<< "BoundingRect Area Height: " << blob.currentBoundingRect.height << "\n" << "BoundingRect Area Width: " << blob.currentBoundingRect.width << "\n"
						<< "DiagonalSize: " << blob.dblCurrentDiagonalSize << "\n";
				}
				//Jesli najpierw byl po prawej, pozniej po lewej i jest powyzej x
				if (blob.centerPositions[prevFrameIndex].x > intHorizontalLinePosition && blob.centerPositions[currFrameIndex].x <= intHorizontalLinePosition && blob.centerPositions[currFrameIndex].y < intBorderLinePosition) {
					carCount[1]++;
					blnAtLeastOneBlobCrossedTheLine = true;
					std::cout << carCount[1] << "\n" << "AspectRatio: " << blob.dblCurrentAspectRatio << "\n" << "BoundingRect Area: " << blob.currentBoundingRect.area() << "\n"
						<< "BoundingRect Area Height: " << blob.currentBoundingRect.height << "\n" << "BoundingRect Area Width: " << blob.currentBoundingRect.width << "\n"
						<< "DiagonalSize: " << blob.dblCurrentDiagonalSize << "\n";
				}
			}

		}
	}

	return blnAtLeastOneBlobCrossedTheLine;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy) {

	for (unsigned int i = 0; i < blobs.size(); i++) {

		if (blobs[i].blnStillBeingTracked == true) {
			cv::rectangle(imgFrame2Copy, blobs[i].currentBoundingRect, SCALAR_RED, 2);

			// id znazlezionego samochodu //////////////////////////////////////
			/*
			int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
			double dblFontScale = blobs[i].dblCurrentDiagonalSize / 60.0;
			int intFontThickness = (int)std::round(dblFontScale * 1.0);
			cv::putText(imgFrame2Copy, std::to_string(i), blobs[i].centerPositions.back(), intFontFace, dblFontScale, SCALAR_GREEN, intFontThickness);
			//*/
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawCarCountOnImage(int carCount[], cv::Mat &imgFrame2Copy) {

	int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
	double dblFontScale = (imgFrame2Copy.rows * imgFrame2Copy.cols) / 300000.0;
	int intFontThickness = (int)std::round(dblFontScale * 1.5);

	cv::Size textSize = cv::getTextSize(std::to_string(carCount[0]), intFontFace, dblFontScale, intFontThickness, 0);

	cv::Point ptText0BottomLeftPosition;
	cv::Point ptText1BottomLeftPosition;

	ptText0BottomLeftPosition.x = imgFrame2Copy.cols - 1 - (int)((double)textSize.width * 1.25);
	ptText0BottomLeftPosition.y = (int)((double)textSize.height * 1.25);

	ptText1BottomLeftPosition.x = 1;
	ptText1BottomLeftPosition.y = (int)((double)textSize.height * 1.25);

	cv::putText(imgFrame2Copy, std::to_string(carCount[0]), ptText0BottomLeftPosition, intFontFace, dblFontScale, SCALAR_GREEN, intFontThickness);
	cv::putText(imgFrame2Copy, std::to_string(carCount[1]), ptText1BottomLeftPosition, intFontFace, dblFontScale, SCALAR_GREEN, intFontThickness);

}









